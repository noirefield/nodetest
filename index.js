const express = require('express');
const AWS = require('aws-sdk');

const app = express();


AWS.config.update({region: 'ap-southeast-1a'});

// Create S3 service object
s3 = new AWS.S3();

// Call S3 to list the buckets
s3.listBuckets(function(err, data) {
  if (err) {
    console.log("Error", err);
  } else {
    console.log("Success", data.Buckets);
  }
});

/*
// HTTP
app.get('/', (req, res, next) => {
    res.send("OK");
    console.log("Request received!");
});

app.listen(80, () => {
    console.log("App started!");
});*/